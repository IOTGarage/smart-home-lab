from kafka import KafkaConsumer
consumer = KafkaConsumer("NetworkTraffic", bootstrap_servers='10.98.44.223:9092')
# remove auto_offset_reset='latest' to start from begining of buffer (last 24 hours)
print (consumer, "Connected to Kafka Server, waiting for messages....")
# Fixed columns 
Cols = ["Line","Time","Source","S-D_Arrow", "Destination","Protocol","Bytes","SourcePort","S-D_Arrow","DestinationPort"]
for msg in consumer:
	# Convert the value into string (currently in bytes format)
	string = msg.value.decode('UTF-8')
	# Split string using NULL charater, first to find its location in the string then split string
	location = string.find(" ")
	character = string[location]
	string = string.split(character)
	D = dict(zip(Cols, string[:9]))
	# As sample print only TCP packets, explore other protocols or addresses etc.
	if (D['Protocol']=='TCP'):
		print (string)
