# Smart Home Lab
  

- [Access Smart Home Data Real-Time](https://gitlab.com/IOTGarage/smart-home-lab/-/tree/main/Access%20Archived%20Smart%20Home%20Data)
  

- [Access Archived Smart Home Data](https://gitlab.com/IOTGarage/smart-home-lab/-/tree/main/Access%20Archived%20Smart%20Home%20Data)
  

- [Access Smart Home Lab Devices List (Miro Board)](https://miro.com/app/board/uXjVMdRRkrc=/?share_link_id=723236734200)
