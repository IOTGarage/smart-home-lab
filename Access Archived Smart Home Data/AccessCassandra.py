from cassandra.cluster import Cluster
from cassandra.auth import PlainTextAuthProvider
import sys
auth_provider = PlainTextAuthProvider(username='roa', password='testing123')
cluster = Cluster(['smarthomelab-cassandradb.cs.cf.ac.uk'], port=9042, auth_provider=auth_provider)
session = cluster.connect()

# Enter DataSource
Data="wireshark"
# Enter Start Timestamp
StartTS = "2023-03-10T11:13:20.100+0000"
# Enter End Timestmap
EndTS = "2023-03-18T11:13:20.100+0000"
#Format "YYYY-MM-DDTHH:i:s.f+GMTD"
try:
    if (sys.argv[1]!=""):
        Data=sys.argv[1]
    if (sys.argv[2]!=""):
        StartTS = sys.argv[2]
        EndTS = sys.argv[3]
except:
    pass
session.execute("USE "+Data)
data = session.execute("SELECT * from data_table WHERE timestamp >= '"+StartTS+"' AND  timestamp <= '"+EndTS+"' ALLOW FILTERING")

with open('output.txt', 'w', encoding='utf-8') as file:
    for row in data:
        print(row.timestamp, row.dump)
        file.write(str(row)+ '\n')

