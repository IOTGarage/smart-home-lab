- Please make sure to install cassandra-driver before running this script using "pip3 install cassandra-driver"

- Run the script using `python3 AccessCassandra.py <DataSource> <StartTime> <EndTime>` (For NetworkTraffic use "wireshark" and for HomeAssistant use "homeassistant" as DataSource)

- TimeStamp Format: "YYYY-MM-DDTHH:i:s.f+GMTD" e.g. "2023-03-17T11:13:20.100+0000"
